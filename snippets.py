# магический метод для ввода данных
#
# a, b, c, = map(int, input().split())
# print(f'a = {a}')
# print(f'b = {b}')
# print(f'c = {c}')

# a = 'fdhbsfjhdbhfjdsbjfbsdjfbsdjfjhsd'
# print(len(a))
# print(a[31])

# a = 'hello'
# # my_list = list(a)
# my_list = [a]
# print(my_list)

# my_list_1 = [1, 2, 3, 4, 5, 6]
# print(my_list)
# #
# my_list.append(1)  # добавить список
# print(my_list)
# a = my_list[1]
# print(a)

# word = 'qwertuyiopfjgkfgjdfjgbfd'
# print(len(word))
# print(word[1:4])
# b = word[1:4]
# a = word[1:12:2]
# print(a)
# c = word[1:20:4]
# print(c)
# my_list = [a, b, c]
# print(my_list)
# my_list.insert(1, my_list_1)
# print(my_list[1])
# h = my_list[1]
# print(h[1])

# my_list = [1, 3, 5, 6, 7, 8, 90, 4, 3, 2, 67, 8, 9]
# # my_list.sort()
# print(my_list)
# print(len(my_list))
# print(my_list[12])

# my_tuple = (23, 34)

# my_list = [2, 'hello', 'hello world', 520]
# print(my_list)
# spisok = list('555gfd69jhgk')
# print(spisok)
# a = int(spisok[0])
# b = int(spisok[1])
# print(a+b)
# print(spisok)

# my_list = [1, 2, 3, 4, 5]
# my_list_2 = [7, 8, 9, 10]
# print(my_list)
# print(my_list_2)
# my_list.append(6)
# print(my_list)
# my_list.extend(my_list_2)
# print(my_list)
# a = my_list[0:9:2]
# print(a)
#
# my_list = [4,7,5,6,1,0,9,3,2,0,9,5,6,4,8,7,3,2,6,7,6,2,8,5,7,8,9]
# my_list.sort()
# print(my_list)
# my_list[1] = 1
# print(my_list)
# tuple_1 = tuple('hello')
# print(tuple_1)
# # tuple_1.append(6)
# tuple_1[1] = 2
# print(tuple_1)
# my_tuple = (0, 0, 1, 2, 2, 2, 3, 9)
# print(my_tuple)
# # # my_tuple[1] = 1
# # # print(my_tuple)
# my_tuple_list = list(my_tuple)
# print(my_tuple_list)
# my_tuple_list[1] = 1
# print(my_tuple_list)
# my_tuple = tuple(my_tuple_list)
# print(my_tuple)

my_dict = {'hello': 1, 'world': 2, 'word': 5, 12: '20'}
# print(my_dict)
# print(my_dict['hello'])
# a = dict(((1,(1,2)), (2,2), (3,3)))
# print(a)
# b = a[1]
# print(b[0] + b[1])
# my_dict['hello'] = 'world'
# print(my_dict)
# # b = len(my_dict['hello'])
# # print(b)
# c = my_dict['hello']
# print(c[0])
# print(my_dict.keys())
# my_list = [1, 2, 3, 4, 5]
# my_list_1 = ["Имя", 3, 4, 5]
# print(f'Рост {my_list_1[0]} = {my_list[2]}cm')
a = set('hellogakwclfjhdsgvhgjhggvhjgjhjhjhjgkhjjhgj')
c = set('sjdhgfhsdvfjsdfjdsjfhjhsdfdsjhfghsjfsdjfjsfj')
# print(a)
# print(c)
# b = {5, 6, 8, 9, 7, 7, 2, 5, 5, 5}
# print(b)
# print(a.intersection(c))

# f1, f2 = 1, 1
# while f2 < 1000:
#     print(f2)
#     next_f2 = f1 + f2
#     next_f1 = f2
#     f1, f2 = next_f1, next_f2
# print(f'{f1}, {f2}')
# c = f1 + f2
# print(c)

# i = 1
# while True:
#     while i < 10:
#         i *= 2
#         print(i)
#     else:
#         print(f'{i} >= 10')
#         i = 1
# print('дотвиданя!')

# my_pets = [ 'cat','dog', 'hamster',]
# b = len(my_pets)
# print(b)
# i = 0
# while i < b:
#     pet = my_pets[i]
#     print('Проверяем ', pet)
#     if pet == 'cat':
#         print('Ура, кот найден!')
#     i += 1
# print('дотвиданя!')
# my_pets = ['lion', 'dog', 'cat', 'skunk', 'hamster', 'cat', 'monkey']
# i = -1
# while i < len(my_pets):
#     i += 1
#     if i == 2:
#         continue
#     pet = my_pets[i]
#     print('Проверяем ', pet)
#     if pet == 'cat':
#         print('Ура, кот найден!')
#         break
# print('дотвиданя!')
#
# f1, f2, count = 0, 1, 0
#
# while f2 < 100000000:
#     count += 1
#     if count > 27:
#         print('Итераций больше чем 27. Прерываюсь.')
#         break
#     f1, f2 = f2, f1 + f2
#     if f2 < 10000:
#         continue
#     print(f2)
# else:
#     print('Было', count, 'итераций')

# while True:
#     user_input = input('Введите 42 >> ')
#     result = int(user_input)
#     if result == 42:
#         print('Спасибо за сотрудничество!')
#         break
#     else:
#         print('Я просил 42, а Вы ввели', result, 'Попробуйте еще раз...')

def some_func():
    print('Привет! Я функция')


some_func()

my_list = [3, 14, 15, 92, 6]
for element in my_list:
    print(element)
    some_func()